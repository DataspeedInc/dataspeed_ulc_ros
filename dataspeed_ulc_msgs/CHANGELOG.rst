^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_ulc_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.1.0 (2022-11-30)
------------------
* Reconfigure CAN and ROS messages for new ULC capability
* Increase CMake minimum version to 3.0.2 to avoid warning about CMP0048
  http://wiki.ros.org/noetic/Migration#Increase_required_CMake_version_to_avoid_author_warning
* Contributors: Kevin Hallenbeck, Micho Radovnikovich

0.0.5 (2019-11-06)
------------------

0.0.4 (2019-05-15)
------------------

0.0.3 (2018-12-09)
------------------

0.0.2 (2018-12-05)
------------------

0.0.1 (2018-11-30)
------------------
* Initial release
* Contributors: Kevin Hallenbeck, Micho Radovnikovich
